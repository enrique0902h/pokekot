# PokeKot



## Getting started

This project contains Login with Gmail, list of all Regions, and list of pokemons per region.

## Clone this project!

You can clone this project and verified the code.

```

git clone git@gitlab.com:enrique0902h/pokekot.git

```

***

## Clone this project!

You can choose from 3 to 6 Pokemons from each Region.

## List of Region available

Kanto
Johto
Hoenn
Sinnoh
Unova
Kalos
Alola
Galar
Hisui
Paldea

## Project status

Unfortunately this project doesn't contains all the requirements of the original project.


## Screenshots

![App Screenshot](https://i.ibb.co/kQ0PGyC/1.png)

![App Screenshot](https://i.ibb.co/jgdh4sF/2.png)

![App Screenshot](https://i.ibb.co/SfkbSqf/3.png)

