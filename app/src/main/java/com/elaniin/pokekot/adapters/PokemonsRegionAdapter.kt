package com.elaniin.pokekot.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.elaniin.pokekot.R
import com.elaniin.pokekot.model.PokemonsRegionEntries

/**
 * Created by Quique on 22/06/2023.
 */
class PokemonsRegionAdapter(val context: Activity, val pokemons: ArrayList<PokemonsRegionEntries>):RecyclerView.Adapter<PokemonsRegionAdapter.PokemonsRegionViewHolder>()  {

    var isSelectedMode:Boolean = false
    var selectedItems= ArrayList<PokemonsRegionEntries>()
    lateinit var onClickListeners: OnClickListeners
    var pokemon: PokemonsRegionEntries?= null
    var pokemonList: List<PokemonsRegionEntries> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonsRegionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon, parent, false)
        return PokemonsRegionViewHolder(view)
    }

    override fun getItemCount(): Int {
        return pokemons.size
    }
    override fun onBindViewHolder(holder: PokemonsRegionViewHolder, position: Int) {
        pokemon = pokemons[position]
        val namePokemon =  pokemon!!.pokemon_species?.name
        val idPokemon =  pokemon!!.entry_number
        holder.textViewName.text = namePokemon
        holder.checkBox.visibility = if (pokemon!!.isSelected) {View.VISIBLE} else {View.GONE}
        holder.checkBox.isChecked = pokemon!!.isSelected

        holder.cvitem.setOnClickListener {
            onClickListeners.onClick(position)
        }

    }

    fun setOnItemLocal(onClickListeners: OnClickListeners ) {
        this.onClickListeners = onClickListeners
    }

   inner class PokemonsRegionViewHolder(view: View): RecyclerView.ViewHolder(view) {
        lateinit var data : PokemonsRegionEntries
        var isSelectedMode:Boolean = false
        val textViewName: TextView
        val cvitem: CardView
        var checkBox: CheckBox
        var onSelection: (() -> Unit?)? = null
        init {
            textViewName = view.findViewById(R.id.textview_name)
            checkBox = view.findViewById(R.id.checkBox)
            cvitem = view.findViewById(R.id.cvitem)
        }
    }

    interface OnClickListeners{
        fun onClick(position: Int)
    }
}
