package com.elaniin.pokekot.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.elaniin.pokekot.R
import com.elaniin.pokekot.activities.PokemonsPerRegionActivity
import com.elaniin.pokekot.model.Details

/**
 * Created by Quique on 22/06/2023.
 */
class RegionsAdapter(val context: Activity, val regions: ArrayList<Details>):RecyclerView.Adapter<RegionsAdapter.RegionsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RegionsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_regions, parent, false)
        return RegionsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return regions.size
    }

    override fun onBindViewHolder(holder: RegionsViewHolder, position: Int) {
        val region = regions[position]
        holder.textViewName.text = region.name

        holder.textViewName.setOnClickListener { goToListPokemon(region) }
    }

    private fun goToListPokemon(region: Details) {
        val i = Intent(context, PokemonsPerRegionActivity::class.java)
        i.putExtra("region", region.toJson())
        i.putExtra("nameRegion", region.name)
        context.startActivity(i)
    }

    class RegionsViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val textViewName: TextView

        init {
            textViewName = view.findViewById(R.id.textview_name)
        }
    }
}