package com.elaniin.pokekot.routes

import com.elaniin.pokekot.model.PokedexRegion
import com.elaniin.pokekot.model.RegionDetails
import com.elaniin.pokekot.model.Regions
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by Quique on 21/06/2023.
 */
interface PokeRoutes  {

    @GET("region/")
    fun getRegionList(): Call<Regions>

    @GET("region/{name_region}/")
    fun getRegionDetail(
        @Path("name_region") nameRegion: String
    ): Call<RegionDetails>

    @GET("pokedex/{name_region}/")
    fun getPokemonPerRegionList(
        @Path("name_region") nameRegion: String
    ): Call<PokedexRegion>


}