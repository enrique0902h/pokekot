package com.elaniin.pokekot.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.elaniin.pokekot.R
import com.elaniin.pokekot.adapters.RegionsAdapter
import com.elaniin.pokekot.model.Regions
import com.elaniin.pokekot.model.Details
import com.elaniin.pokekot.providers.PokeProvider
import com.elaniin.pokekot.utils.SharedPref
import com.google.firebase.auth.FirebaseAuth
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegionActivity : AppCompatActivity() {

    val TAG = "HomeActivity"

    var toolbar: Toolbar? = null
    var btnLogout: Button? = null
    var pokeProvider = PokeProvider()

    var recyclerViewRegions: RecyclerView? = null

    var regionPoke: Regions? = null
    var regionDetails: ArrayList<Details> = ArrayList()
    var adapter: RegionsAdapter? = null
    var sharedPref: SharedPref? = null

    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_region)
        btnLogout = findViewById(R.id.btnLogout)
        sharedPref = SharedPref(this)
        auth = FirebaseAuth.getInstance()
        toolbar = findViewById(R.id.toolbar)
        toolbar?.setTitleTextColor(ContextCompat.getColor(this, R.color.white))
        toolbar?.title = "Regiones"

        val email = intent.getStringExtra("email")


        recyclerViewRegions = findViewById(R.id.recyclerview_regions)
        recyclerViewRegions?.layoutManager = GridLayoutManager(this, 1)

        pokeProvider.pokeRegion()?.enqueue(object : Callback<Regions> {
            override fun onResponse(call: Call<Regions>, response: Response<Regions>) {
                if (response.body() != null) {
                    regionPoke = response.body()!!
                    regionDetails = response.body()?.results!!
                    adapter = RegionsAdapter(this@RegionActivity, regionDetails)
                    recyclerViewRegions?.adapter = adapter
                }
            }

            override fun onFailure(call: Call<Regions>, t: Throwable) {
                Toast.makeText(this@RegionActivity, t.message, Toast.LENGTH_SHORT).show()
                Log.d(TAG, "Error: ${t.message}")
            }
        })

        btnLogout?.setOnClickListener { logout() }
    }

    private fun logout () {
        sharedPref?.remove("email")
        val i = Intent(this, MainActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK // Elimina el historial de pantallas
        startActivity(i)
    }
}