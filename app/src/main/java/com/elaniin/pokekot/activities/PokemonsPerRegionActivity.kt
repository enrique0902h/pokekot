package com.elaniin.pokekot.activities

import android.app.AlertDialog
import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.elaniin.pokekot.R
import com.elaniin.pokekot.adapters.PokemonsRegionAdapter
import com.elaniin.pokekot.adapters.PokemonsRegionAdapter.OnClickListeners
import com.elaniin.pokekot.model.Details
import com.elaniin.pokekot.model.PokedexRegion
import com.elaniin.pokekot.model.PokemonsRegionEntries
import com.elaniin.pokekot.model.RegionDetails
import com.elaniin.pokekot.providers.PokeProvider
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PokemonsPerRegionActivity : AppCompatActivity(), OnClickListeners {
    val TAG = "PokemonsRegion"

    var nameRegion = ""
    var recyclerViewPokemons: RecyclerView? = null
    var btnsave: Button? = null
    var toolbar: Toolbar? = null

    var regionDetails: ArrayList<Details> = ArrayList()
    var pokemonDetails: ArrayList<PokemonsRegionEntries> = ArrayList()
    var pokeProvider = PokeProvider()
    var adapter : PokemonsRegionAdapter? = null
    var selectedItems= ArrayList<PokemonsRegionEntries>()
    var pokemonsSelected= ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pokemons_region)

        nameRegion = intent.getStringExtra("nameRegion").toString()
        btnsave = findViewById(R.id.btnsave)
        toolbar = findViewById(R.id.toolbar)
        toolbar?.setTitleTextColor(ContextCompat.getColor(this, R.color.white))
        toolbar?.title = "Pokemons en la Region ${nameRegion}"
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        recyclerViewPokemons = findViewById(R.id.recyclerview_pokemons)
        recyclerViewPokemons?.layoutManager = GridLayoutManager(this, 3)

//        regionDetail(nameRegion)

        listPokemons(nameRegion)
        btnsave?.setOnClickListener { verifiedPokemon() }

    }

    private fun verifiedPokemon(){
        if(selectedItems.size < 3){
            Toast.makeText(this, "No puedes elegir menos de 3 Pokemons", Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(this, "Se agregaron ${selectedItems.size} Pokemons" , Toast.LENGTH_SHORT).show()
            pokeChoose()
            showPokeDialog()
        }
    }

    private fun pokeChoose(){
        for(p in selectedItems){
            pokemonsSelected.add(p.pokemon_species?.name.toString())
        }
    }

    private fun showPokeDialog(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Pokemons Agregados")
        builder.setMessage("Se Agregaran los siguientes Pokemon ${pokemonsSelected}")

        builder.setPositiveButton(R.string.accept) { dialog, which ->
            Toast.makeText(this, "Gracias por seleccionar", Toast.LENGTH_SHORT).show()
        }
        builder.show()
    }

    private fun listPokemons(nameRegion: String){
        pokeProvider.pokemonsPerRegion(nameRegion)?.enqueue(object : Callback<PokedexRegion>{
            override fun onResponse(call: Call<PokedexRegion>, response: Response<PokedexRegion>) {
                if (response.body() != null) {
                    pokemonDetails = response.body()?.pokemon_entries!!
                    adapter = PokemonsRegionAdapter(this@PokemonsPerRegionActivity, pokemonDetails)
                    recyclerViewPokemons?.adapter = adapter
                    adapter?.setOnItemLocal(this@PokemonsPerRegionActivity)
                }
            }
            override fun onFailure(call: Call<PokedexRegion>, t: Throwable) {
                Toast.makeText(this@PokemonsPerRegionActivity, t.message, Toast.LENGTH_SHORT).show()
                Log.d(TAG, "Error: ${t.message}")
            }

        })
    }

    override fun onClick(position: Int) {
        if(selectedItems.size == 6){
            if(selectedItems.contains(pokemonDetails[position])){
                pokemonDetails[position].isSelected = !pokemonDetails[position].isSelected
                selectedItems.remove(pokemonDetails[position])
            }else{
                Toast.makeText(this, "No puedes elegir más de 6 Pokemons", Toast.LENGTH_SHORT).show()
            }
        }else{
            pokemonDetails[position].isSelected = !pokemonDetails[position].isSelected
            if (pokemonDetails[position].isSelected) {
                selectedItems.add(pokemonDetails[position])
            } else {
                selectedItems.remove(pokemonDetails[position])
            }
        }


        adapter?.notifyItemChanged(position)

    }

}