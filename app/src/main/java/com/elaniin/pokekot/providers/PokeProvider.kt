package com.elaniin.pokekot.providers

import com.elaniin.pokekot.api.ApiRoutes
import com.elaniin.pokekot.model.PokedexRegion
import com.elaniin.pokekot.model.RegionDetails
import com.elaniin.pokekot.model.Regions
import com.elaniin.pokekot.routes.PokeRoutes
import retrofit2.Call

/**
 * Created by Quique on 21/06/2023.
 */
class PokeProvider {

    private var apiRoutes: PokeRoutes? = null

    init {
        val api = ApiRoutes()
        apiRoutes = api.getPokeRegions()
    }

    fun pokeRegion(): Call<Regions>? {
        return apiRoutes?.getRegionList()
    }

    fun regionDetails(nameRegion: String): Call<RegionDetails>? {
        return apiRoutes?.getRegionDetail(nameRegion)
    }

    fun pokemonsPerRegion(nameRegion: String): Call<PokedexRegion>? {
        return apiRoutes?.getPokemonPerRegionList(nameRegion)
    }
}