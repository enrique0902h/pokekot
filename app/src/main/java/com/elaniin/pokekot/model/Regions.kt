package com.elaniin.pokekot.model

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

/**
 * Created by Quique on 21/06/2023.
 */
class Regions(
    @SerializedName("count") val count: Integer? = null,
    @SerializedName("next") var next: String,
    @SerializedName("previous") var previous: String,
    @SerializedName("results") var results: ArrayList<Details>
    ){
    override fun toString(): String {
        return "$count"
    }

    fun toJson(): String {
        return Gson().toJson(this)
    }
}