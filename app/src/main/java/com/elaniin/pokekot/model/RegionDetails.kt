package com.elaniin.pokekot.model

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

/**
 * Created by Quique on 22/06/2023.
 */
class RegionDetails (
    @SerializedName("pokedexes") var pokedexes: ArrayList<Details>
){
    override fun toString(): String {
    return "$pokedexes"
    }

    fun toJson(): String {
        return Gson().toJson(this)
    }
}