package com.elaniin.pokekot.model

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

/**
 * Created by Quique on 22/06/2023.
 */
class PokemonsRegionEntries (
    @SerializedName("entry_number") val entry_number: Int? = null,
    @SerializedName("pokemon_species") var pokemon_species: Details? = null
){
    var isSelected: Boolean = false

    override fun toString(): String {
        return "$pokemon_species"
    }

    fun toJson(): String {
        return Gson().toJson(this)
    }
}