package com.elaniin.pokekot.model

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

/**
 * Created by Quique on 22/06/2023.
 */
class PokedexRegion(
//    @SerializedName("descriptions") val descriptions: ArrayList<String>? = null,
    @SerializedName("id") var id: String,
    @SerializedName("is_main_series") var is_main_series: String,
    @SerializedName("name") var name: String,
//    @SerializedName("names") var names: String,
    @SerializedName("pokemon_entries") var pokemon_entries: ArrayList<PokemonsRegionEntries>,
    @SerializedName("region") var region: Details,
    @SerializedName("version_groups") var version_groups: ArrayList<Details>
){
    override fun toString(): String {
        return "$name"
    }

    fun toJson(): String {
        return Gson().toJson(this)
    }
}