package com.elaniin.pokekot.model

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

/**
 * Created by Quique on 21/06/2023.
 */
class Details (
    @SerializedName("name") var name: String,
    @SerializedName("url") var url: String
){
    override fun toString(): String {
        return "$name"
    }

    fun toJson(): String {
        return Gson().toJson(this)
    }
}