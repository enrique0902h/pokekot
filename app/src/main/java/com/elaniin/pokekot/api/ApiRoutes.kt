package com.elaniin.pokekot.api

import com.elaniin.pokekot.routes.PokeRoutes

/**
 * Created by Quique on 21/06/2023.
 */
class ApiRoutes {

    val API_URL = "https://pokeapi.co/api/v2/"
    val retrofit = RetrofitPoke()

    fun getPokeRegions(): PokeRoutes {
        return retrofit.getClient(API_URL).create(PokeRoutes::class.java)
    }
}